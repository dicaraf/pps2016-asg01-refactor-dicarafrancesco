package controller;

import game.Main;
import model.BasicEntity;
import model.Point;
import model.characters.Turtle;
import utils.Constants;

/**
 * Created by dicaraf on 16/03/2017.
 */
public class TurtleController extends BasicCharacterController implements Runnable{

    private Turtle turtle;

    public TurtleController(Point coordinateTurtle){
        turtle = new Turtle(coordinateTurtle);
        final Thread enemy = new Thread(this);
        enemy.start();
    }

    public Turtle getTurtle(){
        return this.turtle;
    }

    public void moveOnSceneChange() {
        this.turtle.setOffsetX(this.turtle.isMovingToRight() ? Constants.LEFT : Constants.RIGHT);
        this.turtle.setX(this.turtle.getX() + this.turtle.getOffsetX());
    }

    @Override
    public void run() {
        while (true) {
            if (this.turtle.isAlive()) {
                this.moveOnSceneChange();
                try {
                    Thread.sleep(Constants.ENEMY_PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(BasicEntity entity) {
        if (this.hitAhead(this.turtle, entity) && this.turtle.isMovingToRight()) {
            this.turtle.setMovingToRight(false);
            this.turtle.setOffsetX(Constants.RIGHT);
        } else if (this.hitBack(this.turtle, entity) && !this.turtle.isMovingToRight()) {
            this.turtle.setMovingToRight(true);
            this.turtle.setOffsetX(Constants.LEFT);
        }
    }
}
