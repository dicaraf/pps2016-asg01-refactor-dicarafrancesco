package controller;

import model.Point;
import model.characters.BasicCharacter;
import model.characters.Mario;
import model.objects.BasicObject;
import model.objects.Piece;
import game.Main;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by dicaraf on 16/03/2017.
 */
public class MarioController extends BasicCharacterController {

    private Mario mario;

    public MarioController(Point coordinate){
        this.mario = new Mario(coordinate);
    }

    public Mario getMario(){
        return this.mario;
    }

    public Image doJump() {
        String str;

        this.mario.increaseJumpingExtent();

        if (this.mario.getJumpingExtent() < Constants.MARIO_JUMPING_LIMIT) {
            if (this.mario.getY() > Main.getScene().getHeightLimit()) {
                this.mario.setY(this.mario.getY() - 4);
            } else {
                this.mario.setJumpingExtent(Constants.MARIO_JUMPING_LIMIT);
            }

            str = this.mario.isMovingToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else if (this.mario.getY() + this.mario.getHeight() < Main.getScene().getFloorOffsetY()) {
            this.mario.setY(this.mario.getY() + 1);
            str = this.mario.isMovingToRight() ? Res.IMG_MARIO_SUPER_DX : Res.IMG_MARIO_SUPER_SX;
        } else {
            str = this.mario.isMovingToRight() ? Res.IMG_MARIO_ACTIVE_DX : Res.IMG_MARIO_ACTIVE_SX;
            this.mario.setJumping(false);
            this.mario.setJumpingExtent(0);
        }

        return Utils.getImage(str);
    }

    public void contact(BasicObject obj) {
        if (this.hitAhead(this.mario, obj) && this.mario.isMovingToRight() || this.hitBack(this.mario, obj) && !this.mario.isMovingToRight()) {
            Main.getScene().setMov(0);
            this.mario.setMoving(false);
        }

        if (this.hitBelow(this.mario, obj) && this.mario.getJumping()) {
            Main.getScene().setFloorOffsetY(obj.getY());
        } else if (!this.hitBelow(this.mario, obj)) {
            Main.getScene().setFloorOffsetY(Constants.FLOOR_OFFSET_Y_INITIAL);
            if (!this.mario.getJumping()) {
                this.mario.setY(Constants.MARIO_OFFSET_Y_INITIAL);
            }

            if (hitAbove(this.mario, obj)) {
                Main.getScene().setHeightLimit(obj.getY() + obj.getHeight()); // the new sky goes below the object
            } else if (!this.hitAbove(this.mario, obj) && !this.mario.getJumping()) {
                Main.getScene().setHeightLimit(0); // initial sky
            }
        }
    }

    public boolean contactPiece(Piece piece) {
        if (this.hitBack(this.mario, piece) || this.hitAbove(this.mario, piece) || this.hitAhead(this.mario, piece)
                || this.hitBelow(this.mario, piece)) {
            return true;
        }

        return false;
    }

    public void contact(BasicCharacter character) {
        if (this.hitAhead(this.mario, character) || this.hitBack(this.mario, character)) {
            if (character.isAlive()) {
                this.mario.setMoving(false);
                this.mario.setAlive(false);
            } else {
                this.mario.setAlive(true);
            }
        } else if (this.hitBelow(this.mario, character)) {
            character.setMoving(false);
            character.setAlive(false);
        }
    }
}
