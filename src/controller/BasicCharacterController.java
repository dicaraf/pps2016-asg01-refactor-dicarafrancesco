package controller;

import model.*;
import model.characters.BasicCharacter;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

/**
 * Created by dicaraf on 16/03/2017.
 */
public class BasicCharacterController {

    public BasicCharacterController(){
    }


    public Image walk(BasicCharacter character, String name, int frequency) {
        String str = Res.IMG_BASE + name + (!character.isMoving() || character.increaseCounter() % frequency == 0 ? Res.IMGP_STATUS_ACTIVE : Res.IMGP_STATUS_NORMAL) +
                (character.isMovingToRight() ? Res.IMGP_DIRECTION_DX : Res.IMGP_DIRECTION_SX) + Res.IMG_EXT;
        return Utils.getImage(str);
    }

    public boolean hitAhead(BasicCharacter character, BasicEntity entity) {
        return !(character.getX() + character.getWidth() < entity.getX() || character.getX() + character.getWidth() > entity.getX() + Constants.HIT_MARGIN ||
                character.getY() + character.getHeight() <= entity.getY() || character.getY() >= entity.getY() + entity.getHeight());
    }

    protected boolean hitBack(BasicCharacter character, BasicEntity entity) {
        return !(character.getX() > entity.getX() + entity.getWidth() ||
                character.getX() + character.getWidth() < entity.getX() + entity.getWidth() - Constants.HIT_MARGIN ||
                character.getY() + character.getHeight() <= entity.getY() || character.getY() >= entity.getY() + entity.getHeight());
    }

    protected boolean hitBelow(BasicCharacter character, BasicEntity entity) {
        return !(character.getX() + character.getWidth() < entity.getX() + Constants.HIT_MARGIN ||
                character.getX() > entity.getX() + entity.getWidth() - Constants.HIT_MARGIN ||
                character.getY() + character.getHeight() < entity.getY() ||
                character.getY() + character.getHeight() > entity.getY() + Constants.HIT_MARGIN);
    }

    protected boolean hitAbove(BasicCharacter character, BasicEntity entity) {
        return !(character.getX() + character.getWidth() < entity.getX() + Constants.HIT_MARGIN ||
                character.getX() > entity.getX() + entity.getWidth() - Constants.HIT_MARGIN ||
                character.getY() < entity.getY() + entity.getHeight() ||
                character.getY() > entity.getY() + entity.getHeight() + Constants.HIT_MARGIN);
    }

    public boolean isNearby(BasicCharacter character, BasicEntity entity) {
        return (character.getX() > entity.getX() - Constants.PROXIMITY_MARGIN &&
                character.getX() < entity.getX() + entity.getWidth() + Constants.PROXIMITY_MARGIN)
                || (character.getX() + character.getWidth() > entity.getX() - Constants.PROXIMITY_MARGIN &&
                character.getX() + character.getWidth() < entity.getX() + entity.getWidth() + Constants.PROXIMITY_MARGIN);
    }
}
