package controller;

import game.Main;
import model.BasicEntity;
import model.Point;
import model.characters.Mushroom;
import utils.Constants;

/**
 * Created by dicaraf on 16/03/2017.
 */
public class MushroomController extends BasicCharacterController implements Runnable{

    private Mushroom mushroom;

    public MushroomController(Point coordinate){
        mushroom = new Mushroom(coordinate);
        final Thread enemy = new Thread(this);
        enemy.start();
    }

    public Mushroom getMushroom(){
        return this.mushroom;
    }

    public void moveOnSceneChange() {
        this.mushroom.setOffsetX(this.mushroom.isMovingToRight() ? Constants.LEFT : Constants.RIGHT);
        this.mushroom.setX(this.mushroom.getX() + this.mushroom.getOffsetX());
    }

    @Override
    public void run() {
        while (true) {
            if (this.mushroom.isAlive()) {
                this.moveOnSceneChange();
                try {
                    Thread.sleep(Constants.ENEMY_PAUSE);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    public void contact(BasicEntity entity) {
        if (this.hitAhead(this.mushroom, entity) && this.mushroom.isMovingToRight()) {
            this.mushroom.setMovingToRight(false);
            this.mushroom.setOffsetX(Constants.RIGHT);
        } else if (this.hitBack(this.mushroom, entity) && !this.mushroom.isMovingToRight()) {
            this.mushroom.setMovingToRight(true);
            this.mushroom.setOffsetX(Constants.LEFT);
        }
    }
}
