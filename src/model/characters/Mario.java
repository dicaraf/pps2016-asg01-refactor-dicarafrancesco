package model.characters;

import java.awt.Image;

import model.Point;
import utils.Constants;
import utils.Res;
import utils.Utils;

public class Mario extends BasicCharacter {

    private boolean jumping;
    private int jumpingExtent;

    public Mario(Point coordinate) {
        super(coordinate, Constants.MARIO_WIDTH, Constants.MARIO_HEIGHT);
        Image imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
        this.jumping = false;
        this.jumpingExtent = 0;
    }

    public boolean isJumping() {
        return jumping;
    }

    public void setJumping(boolean jumping) {
        this.jumping = jumping;
    }

    public boolean getJumping() {
        return this.jumping;
    }

    public void setJumpingExtent(int jumpingExtent){
        this.jumpingExtent = jumpingExtent;
    }

    public int getJumpingExtent(){
        return this.jumpingExtent;
    }

    public void increaseJumpingExtent(){
        this.jumpingExtent++;
    }


}