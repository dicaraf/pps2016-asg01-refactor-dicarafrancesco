package model.characters;

import model.Point;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

public class Turtle extends BasicEnemy {

    public Turtle(Point coordinate) {
        super(coordinate, Constants.TURTLE_WIDTH, Constants.TURTLE_HEIGHT);
        Image imgTurtle = Utils.getImage(Res.IMG_TURTLE_IDLE);
    }

    public Image deadImage() {
        return Utils.getImage(Res.IMG_TURTLE_DEAD);
    }
}