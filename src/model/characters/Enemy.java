package model.characters;

import java.awt.*;

/**
 * Created by dicaraf on 16/03/2017.
 */
public interface Enemy extends Character {

    Image getImage();

}
