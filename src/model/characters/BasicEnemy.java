package model.characters;

import model.Point;
import utils.Constants;

import java.awt.*;

/**
 * Created by dicaraf on 16/03/2017.
 */
public class BasicEnemy extends BasicCharacter implements Enemy {

    private Image img;
    private int offsetX;

    public BasicEnemy(Point coordinate, int width, int height) {
        super(coordinate, width, height);
        this.setMovingToRight(true);
        this.setMoving(true);
        this.offsetX = Constants.LEFT;
    }

    //getters
    public Image getImage() {
        return img;
    }

    public void setOffsetX(int offsetX){
        this.offsetX = offsetX;
    }

    public int getOffsetX(){
        return offsetX;
    }


}
