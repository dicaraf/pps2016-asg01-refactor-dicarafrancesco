package model.characters;

import model.Entity;

public interface Character extends Entity{

    int getCounter();

    boolean isAlive();

    boolean isMoving();

    boolean isMovingToRight();

    void setAlive(boolean alive);

    void setMoving(boolean moving);

    void setMovingToRight(boolean movingToRight);

    void setCounter(int counter);

}
