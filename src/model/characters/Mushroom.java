package model.characters;

import model.Point;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.*;

public class Mushroom extends BasicEnemy {

    public Mushroom(Point coordinate) {
        super(coordinate, Constants.MUSHROOM_WIDTH, Constants.MUSHROOM_HEIGHT);
        Image img = Utils.getImage(Res.IMG_MUSHROOM_DEFAULT);
    }

    public Image deadImage() {
        return Utils.getImage(this.isMovingToRight() ? Res.IMG_MUSHROOM_DEAD_DX : Res.IMG_MUSHROOM_DEAD_SX);
    }
}