package model.characters;

import model.BasicEntity;
import model.Point;

public class BasicCharacter extends BasicEntity implements Character {

    private boolean moving;
    private boolean movingToRight;
    private int counter;
    private boolean alive;

    public BasicCharacter(Point coordinate, int width, int height) {
        super(coordinate, width, height);
        this.counter = 0;
        this.moving = false;
        this.movingToRight = true;
        this.alive = true;
    }

    public int getCounter() {
        return counter;
    }

    public int increaseCounter(){
        return ++this.counter;
    }

    public boolean isAlive() {
        return alive;
    }

    public boolean isMoving() {
        return moving;
    }

    public boolean isMovingToRight() {
        return movingToRight;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public void setMovingToRight(boolean movingToRight) {
        this.movingToRight = movingToRight;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }



}