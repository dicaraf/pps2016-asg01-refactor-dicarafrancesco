package model.characters;

import controller.MarioController;
import controller.MushroomController;
import controller.TurtleController;
import model.EntityFactory;
import model.Point;
import model.objects.BasicObject;
import model.objects.Piece;

/**
 * Created by dicaraf on 18/03/2017.
 */
public class CharactersFactory extends EntityFactory{
    @Override
    public MarioController getMario(Point coordinate) {
        return new MarioController(coordinate);
    }

    @Override
    public TurtleController getTurtle(Point coordinate) {
        return new TurtleController(coordinate);
    }

    @Override
    public MushroomController getMushroom(Point coordinate) {
        return new MushroomController(coordinate);
    }

    @Override
    public Piece getPiece(Point coordinate) {
        return null;
    }

    @Override
    public BasicObject getBasicObject(String name, Point Coordinate) {
        return null;
    }
}
