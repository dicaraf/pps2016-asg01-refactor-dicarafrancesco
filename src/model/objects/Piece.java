package model.objects;

import model.Point;
import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.Image;

public class Piece extends BasicObject implements Runnable {

    private int counter;

    public Piece(Point coordinate) {
        super(coordinate, Constants.PIECE_WIDTH, Constants.PIECE_HEIGHT);
        super.setImgObj(Utils.getImage(Res.IMG_PIECE1));
    }

    public Image imageOnMovement() {
        return Utils.getImage(++this.counter % Constants.FLIP_FREQUENCY == 0 ? Res.IMG_PIECE1 : Res.IMG_PIECE2);
    }

    @Override
    public void run() {
        while (true) {
            this.imageOnMovement();
            try {
                Thread.sleep(Constants.PIECE_PAUSE);
            } catch (InterruptedException e) {
            }
        }
    }

}