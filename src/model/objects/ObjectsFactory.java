package model.objects;

import controller.MarioController;
import controller.MushroomController;
import controller.TurtleController;
import model.EntityFactory;
import model.Point;
import utils.Constants;

/**
 * Created by dicaraf on 18/03/2017.
 */
public class ObjectsFactory extends EntityFactory {

    @Override
    public MarioController getMario(Point coordinate) {
        return null;
    }

    @Override
    public TurtleController getTurtle(Point coordinate) {
        return null;
    }

    @Override
    public MushroomController getMushroom(Point coordinate) {
        return null;
    }

    @Override
    public Piece getPiece(Point coordinate) {
        return new Piece(coordinate);
    }

    @Override
    public BasicObject getBasicObject(String name, Point coordinate) {

        if(name == null){
            return null;
        }

        if(name.equalsIgnoreCase(Constants.BLOCK)){
            return new Block(coordinate);

        }else if(name.equalsIgnoreCase(Constants.TUNNEL)){
            return new Tunnel(coordinate);
        }

        return null;
    }
}
