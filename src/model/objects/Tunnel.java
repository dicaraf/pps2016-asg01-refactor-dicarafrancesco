package model.objects;

import model.Point;
import utils.Constants;
import utils.Res;
import utils.Utils;

public class Tunnel extends BasicObject {

    public Tunnel(Point coordinate) {
        super(coordinate, Constants.TUNNEL_WIDTH, Constants.TUNNEL_HEIGHT);
        super.setImgObj(Utils.getImage(Res.IMG_TUNNEL));
    }

}
