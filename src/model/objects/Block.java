package model.objects;

import model.Point;
import utils.Constants;
import utils.Res;
import utils.Utils;

public class Block extends BasicObject {

    public Block(Point coordinate) {
        super(coordinate, Constants.BLOCK_WIDTH, Constants.BLOCK_HEIGHT);
        super.setImgObj( Utils.getImage(Res.IMG_BLOCK));
    }

}