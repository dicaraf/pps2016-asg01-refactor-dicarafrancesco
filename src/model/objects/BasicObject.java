package model.objects;

import java.awt.Image;

import model.BasicEntity;
import model.Point;

public class BasicObject extends BasicEntity {

    private Image imgObj;

    public BasicObject(Point coordinate, int width, int height) {
        super(coordinate, width, height);
    }

    public Image getImgObj() {
        return imgObj;
    }

    public void setImgObj(Image imgObj){
        this.imgObj = imgObj;
    }

}