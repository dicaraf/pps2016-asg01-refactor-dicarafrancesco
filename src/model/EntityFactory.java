package model;

import controller.MarioController;
import controller.MushroomController;
import controller.TurtleController;
import model.objects.*;

/**
 * Created by dicaraf on 17/03/2017.
 */
public abstract class EntityFactory {

     public abstract MarioController getMario(Point coordinate);

     public abstract TurtleController getTurtle(Point coordinate);

     public abstract MushroomController getMushroom(Point coordinate);

    public abstract Piece getPiece(Point coordinate);

    public abstract BasicObject getBasicObject(String name, Point Coordinate);

}
