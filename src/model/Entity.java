package model;

/**
 * Created by dicaraf on 16/03/2017.
 */
public interface Entity {

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    void setX(int x);

    void setY(int y);

    void moveOnSceneChange();
}
