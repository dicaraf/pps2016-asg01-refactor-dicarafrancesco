package model;

import game.Main;

/**
 * Created by dicaraf on 16/03/2017.
 */
public class BasicEntity implements Entity {

    private int width, height;
    private Point coordinate;

    public BasicEntity(Point coordinate, int width, int height){
        this.coordinate = coordinate;
        this.width = width;
        this.height = height;
    }

    @Override
    public int getX() {
        return this.coordinate.getX();
    }

    @Override
    public int getY() {
        return this.coordinate.getY();
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public void setX(int x) {
        this.coordinate.setX(x);
    }

    @Override
    public void setY(int y) {
        this.coordinate.setY(y);
    }

    @Override
    public void moveOnSceneChange() {
        if (Main.getScene().getxPos() >= 0) {
            this.setX(this.getX() - Main.getScene().getMov());
        }
    }
}
