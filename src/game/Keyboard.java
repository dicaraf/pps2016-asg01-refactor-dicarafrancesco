package game;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

    @Override
    public void keyPressed(KeyEvent e) {

        if (Main.getScene().getMarioController().getMario().isAlive()) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {

                // per non fare muovere il castello e start
                if (Main.getScene().getxPos() == -1) {
                    Main.getScene().setxPos(0);
                    Main.getScene().setBackground1PosX(-50);
                    Main.getScene().setBackground2PosX(750);
                }
                Main.getScene().getMarioController().getMario().setMoving(true);
                Main.getScene().getMarioController().getMario().setMovingToRight(true);
                Main.getScene().setMov(1); // si muove verso sinistra
            } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {

                if (Main.getScene().getxPos() == 4601) {
                    Main.getScene().setxPos(4600);
                    Main.getScene().setBackground1PosX(-50);
                    Main.getScene().setBackground2PosX(750);
                }

                Main.getScene().getMarioController().getMario().setMoving(true);
                Main.getScene().getMarioController().getMario().setMovingToRight(false);
                Main.getScene().setMov(-1); // si muove verso destra
            }
            // salto
            if (e.getKeyCode() == KeyEvent.VK_UP) {
                Main.getScene().getMarioController().getMario().setJumping(true);
                Audio.playSound("/resources/audio/jump.wav");
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        Main.getScene().getMarioController().getMario().setMoving(false);
        Main.getScene().setMov(0);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

}
