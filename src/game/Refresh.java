package game;

import utils.Constants;

import javax.swing.*;

public class Refresh implements Runnable {

    public void run() {
        while (Main.getScene().getMarioController().getMario().isAlive()) {
            Main.getScene().repaint();
            try {
                Thread.sleep(Constants.PAUSE);
            } catch (InterruptedException e) {
            }
        }
        JOptionPane.showMessageDialog(Main.getScene(),
                "Game Over",
                "Game Over",
                JOptionPane.PLAIN_MESSAGE);
        System.exit(0);
    }

} 
