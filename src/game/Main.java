package game;

import utils.Constants;

import javax.swing.JFrame;

public class Main {

    private static Platform scene;

    public static void main(String[] args) {
        JFrame window = new JFrame(Constants.WINDOW_TITLE);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setSize(Constants.WINDOW_WIDTH, Constants.WINDOW_HEIGHT);
        window.setLocationRelativeTo(null);
        window.setResizable(true);
        window.setAlwaysOnTop(true);

        scene = new Platform();
        window.setContentPane(scene);
        window.setVisible(true);

        Thread timer = new Thread(new Refresh());
        timer.start();
    }

    public static Platform getScene(){
        return scene;
    }

}
