package game;

import utils.Constants;
import utils.Res;
import utils.Utils;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;

import javax.swing.JPanel;

public class Scene extends JPanel {

    private Image imgBackground;
    private Image imgMario;

    public Scene() {
        super();
        this.imgBackground = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgMario = Utils.getImage(Res.IMG_MARIO_DEFAULT);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        g2.drawImage(this.imgBackground, Constants.INITIAL_POSITION_BACKGROUND, Constants.INITIAL_POSITION_BACKGROUND_Y, null);
        g2.drawImage(imgMario, Constants.MARIO_X, Constants.MARIO_Y, null);
    }
}
