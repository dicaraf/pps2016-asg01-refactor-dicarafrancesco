package game;

import controller.BasicCharacterController;
import controller.MarioController;
import controller.MushroomController;
import controller.TurtleController;
import model.EntityFactory;
import model.EntityFactoryProducer;
import model.Point;
import model.characters.Mushroom;
import model.characters.Turtle;
import model.objects.BasicObject;
import model.objects.Piece;
import utils.Constants;
import utils.Res;
import utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Platform extends JPanel {

    private Image imgBackground1;
    private Image imgBackground2;
    private Image castle;
    private Image start;
    private Image imgFlag;
    private Image imgCastle;

    private int background1PosX;
    private int background2PosX;
    private int mov;
    private int xPos;
    private int floorOffsetY;
    private int heightLimit;

    private MarioController mario;
    private MushroomController mushroom;
    private TurtleController turtle;

    private ArrayList<BasicObject> objects;
    private ArrayList<Piece> pieces;

    public Platform() {
        super();
        this.background1PosX = Constants.INITIAL_POSITION_BACKGROUND;
        this.background2PosX = Constants.INITIAL_POSITION_BACKGROUND_2;
        this.mov = 0;
        this.xPos = Constants.INITIAL_POSITION_PLATFORM;
        this.floorOffsetY = Constants.FLOOR_OFFSET_Y_INITIAL;
        this.heightLimit = 0;
        this.imgBackground1 = Utils.getImage(Res.IMG_BACKGROUND);
        this.imgBackground2 = Utils.getImage(Res.IMG_BACKGROUND);
        this.castle = Utils.getImage(Res.IMG_CASTLE);
        this.start = Utils.getImage(Res.START_ICON);


        EntityFactory CharactersFactory = EntityFactoryProducer.getFactory(Constants.CHARACTER);
        mario = CharactersFactory.getMario(new Point(Constants.MARIO_X, Constants.MARIO_Y));
        mushroom = CharactersFactory.getMushroom(new Point(Constants.MUSHROOM_X, Constants.MUSHROOM_Y));
        turtle = CharactersFactory.getTurtle(new Point(Constants.TURTLE_X, Constants.TURTLE_Y));

        this.imgCastle = Utils.getImage(Res.IMG_CASTLE_FINAL);
        this.imgFlag = Utils.getImage(Res.IMG_FLAG);

        EntityFactory ObjectsFactory = EntityFactoryProducer.getFactory(Constants.OBJECTS);
        objects = new ArrayList<>();
        for(int i = 0; i< Constants.BLOCKS_NUMBER; i++){
            this.objects.add(ObjectsFactory.getBasicObject(Constants.BLOCK, Constants.BLOCKS_COORDINATES[i]));
        }
        for (int i = 0; i<Constants.TUNNELS_NUMBER; i++){
            this.objects.add(ObjectsFactory.getBasicObject(Constants.TUNNEL, Constants.TUNNEL_COORDINATES[i]));
        }
        pieces = new ArrayList<>();
        for (int i = 0; i<Constants.PIECES_NUMBER; i++){
            this.pieces.add(ObjectsFactory.getPiece(Constants.PIECES_COORDINATES[i]));
        }
        this.setFocusable(true);
        this.requestFocusInWindow();

        this.addKeyListener(new Keyboard());
    }

    public MarioController getMarioController(){
        return (MarioController) mario;
    }
    public int getFloorOffsetY() {
        return floorOffsetY;
    }

    public int getHeightLimit() {
        return heightLimit;
    }

    public int getMov() {
        return mov;
    }

    public int getxPos() {
        return xPos;
    }

    public void setBackground2PosX(int background2PosX) {
        this.background2PosX = background2PosX;
    }

    public void setFloorOffsetY(int floorOffsetY) {
        this.floorOffsetY = floorOffsetY;
    }

    public void setHeightLimit(int heightLimit) {
        this.heightLimit = heightLimit;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setMov(int mov) {
        this.mov = mov;
    }

    public void setBackground1PosX(int x) {
        this.background1PosX = x;
    }

    public void updateBackgroundOnMovement() {
        if (this.xPos >= 0 && this.xPos <= Constants.FINAL_POSITION_PLATFORM) {
            this.xPos = this.xPos + this.mov;
            // Moving the screen to give the impression that Mario is walking
            this.background1PosX = this.background1PosX - this.mov;
            this.background2PosX = this.background2PosX - this.mov;
        }

        // Flipping between background1 and background2
        if (this.background1PosX == -Constants.FLIPPING_BACKGROUND_X) {
            this.background1PosX = Constants.FLIPPING_BACKGROUND_X;
        }
        else if (this.background2PosX == -Constants.FLIPPING_BACKGROUND_X) {
            this.background2PosX = Constants.FLIPPING_BACKGROUND_X;
        }
        else if (this.background1PosX == Constants.FLIPPING_BACKGROUND_X) {
            this.background1PosX = -Constants.FLIPPING_BACKGROUND_X;
        }
        else if (this.background2PosX == Constants.FLIPPING_BACKGROUND_X) {
            this.background2PosX = -Constants.FLIPPING_BACKGROUND_X;
        }
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics g2 = (Graphics2D) g;

        for (int i = 0; i < objects.size(); i++) {
            if (this.mario.isNearby(this.mario.getMario(), this.objects.get(i))) {
                this.mario.contact(this.objects.get(i));
            }

            if (this.mushroom.isNearby(this.mushroom.getMushroom(), this.objects.get(i))) {
                this.mushroom.contact(this.objects.get(i));
            }

            if (this.turtle.isNearby(this.turtle.getTurtle(), this.objects.get(i))) {
                this.turtle.contact(this.objects.get(i));
            }
        }

        for (int i = 0; i < pieces.size(); i++) {
            if (this.mario.contactPiece(this.pieces.get(i))) {
                Audio.playSound(Res.AUDIO_MONEY);
                this.pieces.remove(i);
            }
        }

        if (this.mushroom.isNearby(this.mushroom.getMushroom(), this.turtle.getTurtle())) {
            this.mushroom.contact(this.turtle.getTurtle());
        }
        if (this.turtle.isNearby(this.turtle.getTurtle(), this.mushroom.getMushroom())) {
            this.turtle.contact(this.mushroom.getMushroom());
        }
        if (this.mario.isNearby(this.mario.getMario(), this.mushroom.getMushroom())) {
            this.mario.contact(this.mushroom.getMushroom());
        }
        if (this.mario.isNearby(this.mario.getMario(), this.turtle.getTurtle())) {
            this.mario.contact(this.turtle.getTurtle());
        }

        // Moving fixed objects
        this.updateBackgroundOnMovement();
        if (this.xPos >= 0 && this.xPos <= Constants.FINAL_POSITION_PLATFORM) {
            for (int i = 0; i < objects.size(); i++) {
                objects.get(i).moveOnSceneChange();
            }

            for (int i = 0; i < pieces.size(); i++) {
                this.pieces.get(i).moveOnSceneChange();
            }

            this.mushroom.moveOnSceneChange();
            this.turtle.moveOnSceneChange();
        }

        g2.drawImage(this.imgBackground1, this.background1PosX, Constants.INITIAL_POSITION_BACKGROUND_Y, null);
        g2.drawImage(this.imgBackground2, this.background2PosX, Constants.INITIAL_POSITION_BACKGROUND_Y, null);
        g2.drawImage(this.castle, Constants.INITIAL_POSITION_CASTLE_X - this.xPos, Constants.INITIAL_POSITION_CASTLE_Y, null);
        g2.drawImage(this.start, Constants.INITIAL_POSITION_START_X - this.xPos, Constants.INITIAL_POSITION_START_Y, null);

        for (int i = 0; i < objects.size(); i++) {
            g2.drawImage(this.objects.get(i).getImgObj(), this.objects.get(i).getX(),
                    this.objects.get(i).getY(), null);
        }

        for (int i = 0; i < pieces.size(); i++) {
            g2.drawImage(this.pieces.get(i).imageOnMovement(), this.pieces.get(i).getX(),
                    this.pieces.get(i).getY(), null);
        }

        g2.drawImage(this.imgFlag, Constants.FLAG_X_POS - this.xPos, Constants.FLAG_Y_POS, null);
        g2.drawImage(this.imgCastle, Constants.CASTLE_X_POS - this.xPos, Constants.CASTLE_Y_POS, null);

        if (this.mario.getMario().isJumping()) {
            g2.drawImage(this.mario.doJump(), this.mario.getMario().getX(), this.mario.getMario().getY(), null);
        } else {
            g2.drawImage(this.mario.walk(this.mario.getMario(), Res.IMGP_CHARACTER_MARIO, Constants.MARIO_FREQUENCY),
                    this.mario.getMario().getX(), this.mario.getMario().getY(), null);
        }

        if (this.mushroom.getMushroom().isAlive()) {
            g2.drawImage(this.mushroom.walk(this.mushroom.getMushroom(), Res.IMGP_CHARACTER_MUSHROOM, Constants.MUSHROOM_FREQUENCY),
                    this.mushroom.getMushroom().getX(), this.mushroom.getMushroom().getY(), null);
        } else {
            g2.drawImage(this.mushroom.getMushroom().deadImage(), this.mushroom.getMushroom().getX(), this.mushroom.getMushroom().getY() + Constants.MUSHROOM_DEAD_OFFSET_Y, null);
        }

        if (this.turtle.getTurtle().isAlive()) {
            g2.drawImage(this.turtle.walk(this.turtle.getTurtle(), Res.IMGP_CHARACTER_TURTLE, Constants.TURTLE_FREQUENCY), this.turtle.getTurtle().getX(), this.turtle.getTurtle().getY(), null);
        } else {
            g2.drawImage(this.turtle.getTurtle().deadImage(), this.turtle.getTurtle().getX(), this.turtle.getTurtle().getY() + Constants.TURTLE_DEAD_OFFSET_Y, null);
        }
    }
}
