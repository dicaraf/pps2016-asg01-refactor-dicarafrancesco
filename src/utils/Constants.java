package utils;

import model.Point;

/**
 * Created by dicaraf on 16/03/2017.
 */
public class Constants {

    //FACTORY
    public static final String BLOCK =  "block";
    public static final String TUNNEL =  "tunnel";
    public static final String CHARACTER =  "character";
    public static final String OBJECTS =  "objects";

    //MARIO
    public static final int MARIO_WIDTH = 28;
    public static final int MARIO_HEIGHT = 50;
    public static final int MARIO_JUMPING_LIMIT = 42;
    public static final int MARIO_OFFSET_Y_INITIAL = 243;

    //ENEMIES
    public static final int MUSHROOM_WIDTH = 27;
    public static final int MUSHROOM_HEIGHT = 30;
    public static final int TURTLE_WIDTH = 43;
    public static final int TURTLE_HEIGHT = 50;

    //BASICCARACHTER
    public static final int PROXIMITY_MARGIN = 10;
    public static final int HIT_MARGIN =5;

    //BASICENEMY
    public static final int ENEMY_PAUSE = 15;
    public static final int LEFT =1;
    public static final int RIGHT=-1;

    //BLOCK
    public static final int BLOCK_WIDTH = 30;
    public static final int BLOCK_HEIGHT = 30;

    //PIECE
    public static final int PIECE_WIDTH = 30;
    public static final int PIECE_HEIGHT = 30;
    public static final int PIECE_PAUSE = 10;
    public static final int FLIP_FREQUENCY = 100;

    //TUNNEL
    public static final int TUNNEL_WIDTH = 43;
    public static final int TUNNEL_HEIGHT = 65;

    //MAIN
    public static final int WINDOW_WIDTH = 700;
    public static final int WINDOW_HEIGHT = 360;
    public static final String WINDOW_TITLE = "Super Mario";

    //REFRESH
    public static final int PAUSE = 3;

    //PLATFORM
    public static final int MARIO_X = 300;
    public static final int MARIO_Y = 245;
    public static final int MUSHROOM_X= 800;
    public static final int MUSHROOM_Y= 263;
    public static final int TURTLE_X= 950;
    public static final int TURTLE_Y= 243;
    public static final int INITIAL_POSITION_PLATFORM = -1;
    public static final int FINAL_POSITION_PLATFORM = 4600;
    public static final int INITIAL_POSITION_BACKGROUND_Y = 0;
    public static final int INITIAL_POSITION_BACKGROUND = -50;
    public static final int INITIAL_POSITION_BACKGROUND_2 = 750;
    public static final int INITIAL_POSITION_CASTLE_X = 10;
    public static final int INITIAL_POSITION_CASTLE_Y = 95;
    public static final int INITIAL_POSITION_START_X = 220;
    public static final int INITIAL_POSITION_START_Y = 234;
    public static final int FLOOR_OFFSET_Y_INITIAL = 293;
    public static final int MARIO_FREQUENCY = 25;
    public static final int MUSHROOM_FREQUENCY = 45;
    public static final int TURTLE_FREQUENCY = 45;
    public static final int MUSHROOM_DEAD_OFFSET_Y = 20;
    public static final int TURTLE_DEAD_OFFSET_Y = 30;
    public static final int FLAG_X_POS = 4650;
    public static final int CASTLE_X_POS = 4850;
    public static final int FLAG_Y_POS = 115;
    public static final int CASTLE_Y_POS = 145;
    public static final int FLIPPING_BACKGROUND_X = 800;
    public static final int BLOCKS_NUMBER = 12;
    public static final Point[] BLOCKS_COORDINATES = {

            new Point(400, 180),
            new Point(1200, 180),
            new Point(1270, 170),
            new Point(1340, 160),
            new Point(2000, 180),
            new Point(2600, 160),
            new Point(2650, 180),
            new Point(3500, 160),
            new Point(3550, 140),
            new Point(4000, 170),
            new Point(4200, 200),
            new Point(4300, 210),

    };

    public static final int PIECES_NUMBER = 10;
    public static final Point[] PIECES_COORDINATES = {
            new Point(402, 145),
            new Point(1202, 140),
            new Point(1272, 95),
            new Point(1342, 40),
            new Point(1650, 145),
            new Point(2650, 145),
            new Point(3000, 135),
            new Point(3400, 125),
            new Point(4200, 145),
            new Point(4600, 40),

    };

    public static final int TUNNELS_NUMBER = 8;
    public static final Point[] TUNNEL_COORDINATES = {
            new Point(600, 230),
            new Point(1000, 230),
            new Point(1600, 230),
            new Point(1900, 230),
            new Point(2500, 230),
            new Point(3000, 230),
            new Point(3800, 230),
            new Point(4500, 230),

    };

}
